package com.qmetry.qaf.example.test;

import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;

import com.qmetry.qaf.example.pages.AddTravelRequestPage;
import com.qmetry.qaf.example.pages.ExpenseRequestApprovalPage;
import com.qmetry.qaf.example.pages.Homepage;
import com.qmetry.qaf.example.pages.LoginPage;

public class ExpenceInvalideDataTest extends WebDriverTestCase{

	Homepage home=new Homepage();
	 LoginPage lin=new LoginPage();
    ExpenseRequestApprovalPage expencePage=new ExpenseRequestApprovalPage();
    AddTravelRequestPage addTravel=new AddTravelRequestPage();
   
    @Test
     public void verifyInvalidDetailsOnNewExpensePage() {
    	
	    lin.launchBrowser();
    	lin.enterValidCredential("ashish.biradar1","Test@1234");
	    home.clickOnNavigable();
	    expencePage.clickOnReimbursement();
	    expencePage.clickOnMyExpenceList();
	    expencePage.clickOnNewExpenceButton();
	    expencePage.fillInvalideDetails();
	    lin.logOutNest();
    	
 } 
 
   
}
