package com.qmetry.qaf.example.test;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;

import com.qmetry.qaf.example.pages.Homepage;
import com.qmetry.qaf.example.pages.LoginPage;
import com.qmetry.qaf.example.pages.TeamLeaveListPage;

public class TeamLeaveListTest extends WebDriverTestCase{
	 
 
	@Test
	public void verifyTeamLeaveListPage(){
		try{
		Homepage home=new Homepage();
		 LoginPage lin=new LoginPage();
		 TeamLeaveListPage leavepage=new TeamLeaveListPage();
		   lin.launchBrowser();
		   lin.enterValidCredential(getProps().getString("username.property"),getProps().getString("password.property"));
		   home.clickOnMyView();
		   home.clickOnNavigable();
		   leavepage.clickOnTeamLeaveList();
		   leavepage.verifycoloum(getProps().getString("coloumn.content.leavelistpage"));
		   leavepage.submitAndApproveButton();
		   lin.logOutNest();
		}catch(Exception e){
			   Reporter.log("msg");
		   }
	}
}
