package com.qmetry.qaf.example.test;



import org.testng.Reporter;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;

import com.qmetry.qaf.example.pages.LeaveHolidayPage;
import com.qmetry.qaf.example.pages.LoginPage;

public class LeaveHolidayTest extends WebDriverTestCase{
	
	
	@Test
	public void verifyLeaveIncludingHoliday(){
		try{
		LeaveHolidayPage holiday=new LeaveHolidayPage();
		LoginPage lin=new LoginPage();
		lin.launchBrowser();
		lin.enterValidCredential(getProps().getString("username.property"),getProps().getString("password.property"));
	    holiday.clickOnNavigable();
		holiday.clickOnLeaveModule();
		holiday.clickOnApplyLeaveModule();
		holiday.selectDate();
		holiday.leaveReason();
		holiday.clickOnApply();
		holiday.getnotification();
		lin.logOutNest();
		}catch(Exception e){
			   Reporter.log("msg");
		   }
		
	}

}
