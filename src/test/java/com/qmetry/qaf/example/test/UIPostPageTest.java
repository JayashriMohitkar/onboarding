package com.qmetry.qaf.example.test;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;

import com.qmetry.qaf.example.pages.Homepage;
import com.qmetry.qaf.example.pages.LoginPage;
import com.qmetry.qaf.example.pages.UIpostPage;


public class UIPostPageTest extends WebDriverTestCase{
	
	
	 
	 @Test
	 public void verifyUIOfMyPostPage(){
		 try{
		 Homepage home=new Homepage();
		 UIpostPage postpage=new UIpostPage();
		 LoginPage lin=new LoginPage();
		 lin.launchBrowser();
		 lin.enterValidCredential(getProps().getString("HR.username.property"),getProps().getString("HR.password.property"));
		 home.clickOnMyView();
		 home.clickOnNavigable();
		 postpage.clickOnPublisher();
		 postpage.clickOnMyPost();
		 postpage.verifyPostUIPage(getProps().getString("table.row.property"));
		 postpage.verifylabels(getProps().getString("labellist.property"));
		 postpage.pagetitle();
		 postpage.verifypagination();
		 postpage.verifyCreateButton();
		 postpage.actionColumn();
		 postpage.verifySearchResetButton();
		 postpage.verifyHidenIcon();
		 postpage.bredcrumbs();
		 lin.logOutNest();
		 }catch(Exception e){
			   Reporter.log("msg");
		   }
		}
		 
	 


}
