package com.qmetry.qaf.example.test;



import org.testng.Reporter;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;

import com.qmetry.qaf.example.pages.ExpenseRequestApprovalPage;
import com.qmetry.qaf.example.pages.Homepage;
import com.qmetry.qaf.example.pages.LoginPage;

public class AddFileExpenceRequestTest extends WebDriverTestCase{
	
	
   
   @Test
    public void expRequestWithFileAttachment() throws Exception{
	   try{
	   Homepage home=new Homepage();
		 LoginPage lin=new LoginPage();
	     ExpenseRequestApprovalPage expencePage=new ExpenseRequestApprovalPage();
	     lin.launchBrowser();
	    lin.enterValidCredential(getProps().getString("username.property"),getProps().getString("password.property"));
	    home.clickOnNavigable();
	    expencePage.clickOnReimbursement();
	    expencePage.clickOnMyExpenceList();
	    expencePage.clickOnNewExpenceButton();
	    expencePage.fillDataWithFileAttachment();
	    lin.logOutNest();
	   }catch(Exception e){
		   Reporter.log("msg");
	   }
   
   }
   
  
   
}