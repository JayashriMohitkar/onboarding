
package com.qmetry.qaf.example.test;


import org.testng.Reporter;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;

import com.qmetry.qaf.example.pages.BrightSparkPage;
import com.qmetry.qaf.example.pages.Homepage;
import com.qmetry.qaf.example.pages.LoginPage;
import com.qmetry.qaf.example.pages.MyNominationPage;

public class MyNominationTest extends WebDriverTestCase{
	
	
	 
	 @Test
	 public void verifyTheDetailofMyNominations(){
		 try{
		 BrightSparkPage brightspark=new BrightSparkPage();
		 Homepage home=new Homepage();
		 LoginPage lin=new LoginPage();
		 MyNominationPage nomineepage=new  MyNominationPage();
		 lin.launchBrowser();
		 lin.enterValidCredential(getProps().getString("username.property"),getProps().getString("password.property"));
		   home.clickOnNavigable();
		   brightspark.scrollDownForRandR();
		   nomineepage.clickOnMyRnR();
		   nomineepage.clickOnMyNominations();
		   nomineepage.verifyBreadcrumb();
		   nomineepage.verifyTablecolumn(getProps().getString("mynomination.property"));
		   nomineepage.verifyPagination();
		   nomineepage.clickOnPluseIcon();
		  nomineepage.verifycolorcode(getProps().getString("green.approved.property"));
		  nomineepage.verifycolorcode(getProps().getString("yellow.pending.property"));
		  nomineepage.verifycolorcode(getProps().getString("redcolor.rejectstatus.property"));
		  lin.logOutNest();
		 }catch(Exception e){
			   Reporter.log("msg");
		   }
		
      }
 }
