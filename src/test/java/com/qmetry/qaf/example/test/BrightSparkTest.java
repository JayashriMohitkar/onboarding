package com.qmetry.qaf.example.test;


import org.testng.Reporter;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.example.pages.BrightSparkPage;
import com.qmetry.qaf.example.pages.Homepage;
import com.qmetry.qaf.example.pages.LoginPage;

public class BrightSparkTest extends WebDriverTestCase{
	
	
	
   @Test
   public void verifyApprovalforBrightSpark() {
	   try{
	    BrightSparkPage brightspark=new BrightSparkPage();
		 Homepage home=new Homepage();
		 LoginPage lin=new LoginPage();
	    lin.launchBrowser();
	    lin.enterValidCredential(getProps().getString("username.property"),getProps().getString("password.property"));
	    home.clickOnNavigable();
	    brightspark.scrollDownForRandR();
	    brightspark.clickOnNominateModule();
	    brightspark.clickOnSparkModule();
	    brightspark.nomineeVirat();;
	    lin.logOutNest();
	   lin.enterValidCredential(getProps().getString("ManagerAdmin.username.property"),getProps().getString("ManagerAdmin.password.property"));
	   home.clickOnMyView();
	   home.clickOnNavigable();
	   brightspark.verifyList();
	   brightspark.verifyaprroveReject();
	   lin.logOutNest();
	   }catch(Exception e){
		   Reporter.log("msg");
	   }
	 }
   
   @Test
   public void verifydetailofTheBrightSparkPage(){
	   try{
 	  BrightSparkPage brightspark=new BrightSparkPage();
 		 Homepage home=new Homepage();
 		 LoginPage lin=new LoginPage();
 		 lin.launchBrowser();
 	   lin.enterValidCredential(getProps().getString("username.property"),getProps().getString("password.property"));
 	   home.clickOnNavigable();
 	   brightspark.scrollDownForRandR();
 	   brightspark.clickOnNominateModule();
 	   brightspark.clickOnSparkModule();
 	   brightspark.nomineeVirat();
 	   lin.logOutNest();
 	   lin.enterValidCredential(getProps().getString("ManagerAdmin.username.property"),getProps().getString("ManagerAdmin.password.property"));
 	   home.clickOnMyView();
 	   home.clickOnNavigable();
 	   brightspark.verifyList();
 	   brightspark.clickOnApprovedButton();
 	   lin.logOutNest();
 	   lin.enterValidCredential(getProps().getString("HR.username.property"), getProps().getString("HR.username.property"));
 	   home.clickOnMyView();
 	   home.clickOnNavigable();
 	   brightspark.clickOnRnR();
 	   brightspark.clickOnApprovedButton();
 	   brightspark.clickOnListBrightSpark();
 	   brightspark.verifyRow(getProps().getString("brightsparkrow.property"));
 	   brightspark.verifyValueAdditionContent(getProps().getString("valueAdditions.property"));
 	   brightspark.rewardCardImage();
 	   brightspark.verifyBackButton();
 	   lin.logOutNest();
	   }catch(Exception e){
		   Reporter.log("msg");
	   }
 	  }
   
   @Test
   public void verifyApprovalProcessofBrightSpark(){
 	  BrightSparkPage brightspark=new BrightSparkPage();
 		 Homepage home=new Homepage();
 		 LoginPage lin=new LoginPage();
 	   lin.launchBrowser();
 	   lin.enterValidCredential(getProps().getString("username.property"),getProps().getString("password.property"));
 	   home.clickOnNavigable();
 	   brightspark.scrollDownForRandR();
 	   brightspark.clickOnNominateModule();
 	   brightspark.clickOnSparkModule();
 	   brightspark.nomineeVirat();
 	   lin.logOutNest();
 	   lin.enterValidCredential(getProps().getString("ManagerAdmin.username.property"),getProps().getString("ManagerAdmin.password.property"));
 	   home.clickOnMyView();
 	   home.clickOnNavigable();
 	   brightspark.verifyList();
 	   brightspark.clickOnRejectButton();
 	   brightspark.clickOnRejectStatus();
 	   brightspark.verifyRow(getProps().getString("brightsparkrow.property"));
 	   brightspark.verifyValueAdditionContent(getProps().getString("valueAdditions.property"));
 	   brightspark.rewardCardImage();
 	   brightspark.verifyBackButton();
 	   lin.logOutNest();
   
 		 
    
 	   
  }
   @Test
   public void approvalForBrightSparkNominee() {
	   try{
 	  BrightSparkPage brightspark=new BrightSparkPage();
 	 Homepage home=new Homepage();
 		 LoginPage lin=new LoginPage();
 	  lin.launchBrowser();
 	  lin.enterValidCredential(getProps().getString("username.property"),getProps().getString("password.property"));
 	   home.clickOnNavigable();
 	   brightspark.scrollDownForRandR();
 	   brightspark.clickOnNominateModule();
 	   brightspark.clickOnSparkModule();
 	   brightspark.nomineeVirat();
 	   lin.logOutNest();
 	   lin.enterValidCredential(getProps().getString("ManagerAdmin.username.property"),getProps().getString("ManagerAdmin.password.property"));
 	   home.clickOnMyView();
 	   home.clickOnNavigable();
 	   brightspark.clickOnRandRManagerView();;
 	   lin.logOutNest();
 	   lin.enterValidCredential(getProps().getString("Deliverymanager.uname.property"),getProps().getString("Deliverymanager.uname.property"));
 	   home.clickOnMyView();
 	   home.clickOnNavigable();
 	   brightspark.observeListRnRList();
 	   lin.logOutNest();
	   }catch(Exception e){
		   Reporter.log("msg");
	   }
   }
   
}
