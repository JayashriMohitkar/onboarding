package com.qmetry.qaf.example.test;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;

import com.qmetry.qaf.example.pages.BrightSparkPage;
import com.qmetry.qaf.example.pages.Homepage;
import com.qmetry.qaf.example.pages.LoginPage;
import com.qmetry.qaf.example.pages.PatOnBackPage;

public class PatOnBackTest extends WebDriverTestCase{
	
	
	
	 
	 @Test
	 public void verifyTheApprovalProcessofPatOnBack(){
		 try{
		 BrightSparkPage brightspark=new BrightSparkPage();
		 Homepage home=new Homepage();
		 LoginPage lin=new LoginPage();
		 PatOnBackPage patonpage=new  PatOnBackPage();
		 lin.launchBrowser();
		   lin.enterValidCredential(getProps().getString("username.property"),getProps().getString("password.property"));
		   home.clickOnNavigable();
		   brightspark.scrollDownForRandR();
		   brightspark.clickOnNominateModule();
		   patonpage.clickOnPatOnBack();
		   patonpage.fillDataOnPatOnPage();
		   lin.logOutNest();
		   lin.enterValidCredential(getProps().getString("ManagerAdmin.username.property"),getProps().getString("ManagerAdmin.password.property"));
		   home.clickOnMyView();
		   home.clickOnNavigable();
		   brightspark.clickOnRandRManagerView();;
		   lin.logOutNest();
		   lin.enterValidCredential(getProps().getString("Deliverymanager.uname.property"),getProps().getString("Deliverymanager.uname.property"));
		   home.clickOnMyView();
		   home.clickOnNavigable();
		   brightspark.observeListRnRList();
		   lin.logOutNest();
		 }catch(Exception e){
			   Reporter.log("msg");
		   }
	 }
	

}
