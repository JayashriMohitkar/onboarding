package com.qmetry.qaf.example.test;


import org.testng.Reporter;
import org.testng.annotations.Test;


import com.qmetry.qaf.automation.ui.WebDriverTestCase;

import com.qmetry.qaf.example.pages.AddTravelRequestPage;

import com.qmetry.qaf.example.pages.Homepage;
import com.qmetry.qaf.example.pages.LoginPage;


public class AddTravelRequestTest extends WebDriverTestCase {
     
	
	
	
	 @Test
	 public void verifyTheAddTravelRequestPage() {
		 try{
		 Homepage home=new Homepage();
		 LoginPage lin=new LoginPage();
		 AddTravelRequestPage addTravel=new AddTravelRequestPage();
	     lin.launchBrowser();
		 lin.enterValidCredential(getProps().getString("username.property"),getProps().getString("password.property"));
		 home.clickOnNavigable();
		 addTravel.clickOnTravelMenu();
		 addTravel.verifyAddTravelRequestPage(getProps().getString("allfield.property"));
		 lin.logOutNest();
	 }catch(Exception e){
		   Reporter.log("msg");
	   }
	 }
	 
	 

}
