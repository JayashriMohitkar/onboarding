package com.qmetry.qaf.example.test;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;

import com.qmetry.qaf.example.pages.Homepage;
import com.qmetry.qaf.example.pages.LoginPage;

public class SingleDayLeaveTest extends WebDriverTestCase{
	
	@Test
	public void verifyForSingleDayLeave(){
		try{
		LoginPage lin=new LoginPage();
		Homepage home=new Homepage();
	    lin.launchBrowser();
		lin.enterValidCredential(getProps().getString("username.property"),getProps().getString("password.property"));
		home.clickOnNavigable();
		home.clickOnLeaveModule();
		home.clickOnApplyLeaveModule();
		home.dropDown();
		home.dayType();
		home.clickOnApply();
		home.verifySuccessfullyApplyToLeave();
		lin.logOutNest();
		}catch(Exception e){
			   Reporter.log("msg");
		   }
	}

}
