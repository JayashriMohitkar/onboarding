package com.qmetry.qaf.example.test;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;

import com.qmetry.qaf.example.pages.Homepage;
import com.qmetry.qaf.example.pages.LoginPage;
import com.qmetry.qaf.example.pages.NewPostUIPage;
import com.qmetry.qaf.example.pages.UIpostPage;

public class NewUIPostTest extends WebDriverTestCase{

	
			
	  @Test
	 public void verifyUIforNewPostPage(){
		  try{
		Homepage home=new Homepage();
	  UIpostPage postpage=new UIpostPage();
	   LoginPage lin=new LoginPage();
		NewPostUIPage newpost=new NewPostUIPage();
		 lin.launchBrowser();
		 lin.enterValidCredential(getProps().getString("HR.username.property"),getProps().getString("HR.password.property"));
		 home.clickOnMyView();
		 home.clickOnNavigable();
		 postpage.clickOnPublisher();
		 postpage.clickOnMyPost();
		 postpage.clickOnCreateButton();
		 newpost.verifyTitle();
		 newpost.verifyTextField();
		 newpost.verifyDropDown();
		 lin.logOutNest();
		  }catch(Exception e){
			   Reporter.log("msg");
		   }
   }

}
