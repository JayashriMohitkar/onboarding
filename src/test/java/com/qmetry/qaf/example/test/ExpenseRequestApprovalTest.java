package com.qmetry.qaf.example.test;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverTestCase;

import com.qmetry.qaf.example.pages.ExpenseRequestApprovalPage;
import com.qmetry.qaf.example.pages.Homepage;
import com.qmetry.qaf.example.pages.LoginPage;

public class ExpenseRequestApprovalTest extends WebDriverTestCase{
	
	
  @Test
  public void verifyfunctionalityofExpenseRequestApproval() {
	  try{
        Homepage home=new Homepage();
		 LoginPage lin=new LoginPage();
		 ExpenseRequestApprovalPage expencePage=new ExpenseRequestApprovalPage();
	    lin.launchBrowser();
	    lin.enterValidCredential(getProps().getString("emp.uname.property"),getProps().getString("emp.pwd.property"));
	    home.clickOnNavigable();
	    expencePage.clickOnReimbursement();
	    expencePage.clickOnMyExpenceList();
	    expencePage.clickOnNewExpenceButton();
	    expencePage.fillValideDetails();
	    lin.logOutNest();
	    lin.enterValidCredential(getProps().getString("promanager.uname.property"),getProps().getString("promanager.pwd.property"));
	    home.clickOnMyView();
	    expencePage.expenceRequest();
	    lin.logOutNest();
	    lin.enterValidCredential(getProps().getString("emp.uname.property"),getProps().getString("emp.pwd.property"));
		home.clickOnNavigable();
	    expencePage.redirectedtoMyExpenseList();
	   lin.logOutNest();
	  }catch(Exception e){
		   Reporter.log("msg");
	   }
	} 
   @Test
   public void verifyInvalidDetailsOnNewExpensePage() {
	   try{
 	   Homepage home=new Homepage();
	   LoginPage lin=new LoginPage();
       ExpenseRequestApprovalPage expencePage=new ExpenseRequestApprovalPage();
       lin.launchBrowser();
 	    lin.enterValidCredential(getProps().getString("username.property"),getProps().getString("password.property"));
	    home.clickOnNavigable();
	    expencePage.clickOnReimbursement();
	    expencePage.clickOnMyExpenceList();
	    expencePage.clickOnNewExpenceButton();
	    expencePage.fillInvalideDetails();
	    lin.logOutNest();
	   }catch(Exception e){
		   Reporter.log("msg");
	   }
} 


}
