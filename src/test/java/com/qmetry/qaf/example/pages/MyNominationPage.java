package com.qmetry.qaf.example.pages;

import static com.qmetry.qaf.automation.step.CommonStep.*;



import org.hamcrest.Matchers;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.Color;


import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class MyNominationPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	 
	
	
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		
		
	}
	
	 public  void clickOnMyRnR(){
		 JavascriptExecutor jse=(JavascriptExecutor) new WebDriverTestCase().getDriver();
		 QAFWebElement myRnR=new QAFExtendedWebElement("myRnRList.loc");
		 jse.executeScript("arguments[0].scrollIntoView();", myRnR);
		 jse.executeScript("arguments[0].click();",myRnR);
		
		
	 }
	 
	 
	 public  void clickOnMyNominations(){
		 JavascriptExecutor jse=(JavascriptExecutor) new WebDriverTestCase().getDriver();
		 QAFWebElement mynomination=new QAFExtendedWebElement("mynominations.loc");
		 jse.executeScript("arguments[0].scrollIntoView();", mynomination);
		 jse.executeScript("arguments[0].click();",mynomination);
		 waitForNotVisible("login1.loading.loader");
	 }
	 
	 public void clickOnPluseIcon(){
		 click("pluse.icon.loc");
	 }
	 
	 public  void verifyBreadcrumb(){
		 Validator.verifyThat("Verifying breadcrumb", CommonStep.getText("breadcrumb.loc"),
	                Matchers.containsString("Home /  R & R /  My R & R")); 
		 
	 }
	 
     public void verifycolorcode(String color){
		 verifyPresent("color.loc");
		 String hex =Color.fromString(color).asHex();
		if(hex.equalsIgnoreCase("#56b05e")){
			verifyPresent("color.loc");
			Reporter.log("Approved status is in Green");
		}else if(hex.equalsIgnoreCase("#efbb40")){
			verifyPresent("color.loc");
			Reporter.log("Pendind status is in Yellow");
		}else if(hex.equalsIgnoreCase("#ea6c6c")){
			verifyPresent("color.loc");
			Reporter.log("Reject status is in Red");
		}
	}
	 
	 public  void verifyTablecolumn(String elements){
		 String[] coloumnlist= elements.split(",");
			for(int i=0;i<coloumnlist.length;i++){
				Validator.verifyThat(new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("tableMynomination.loc"),coloumnlist[i])).isPresent(),Matchers.equalTo(true) );
				com.qmetry.qaf.automation.util.Reporter.logWithScreenShot("Verify table content");
		     }
	 }
	 
	 public void verifyPagination(){
	     verifyPresent("pagination.loc");
	 }
	 

}
