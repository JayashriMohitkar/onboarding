package com.qmetry.qaf.example.pages;

import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.example.utilities.CommonUtils;


public class LoginPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	CommonUtils commonutil=new CommonUtils();
	 
	@FindBy(locator="login1.userName.txt")
	public QAFWebElement username;
	
	@FindBy(locator="login1.Password.txt")
	public QAFWebElement password;
	
	@FindBy(locator="login1.Logbutton.button")
	public QAFWebElement loginbutton;
	
	@FindBy(locator="logout.loc")
	public QAFWebElement logout;
    
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");
		driver.manage().window().maximize();
		driver.navigate().refresh();
	}
	 public void launchBrowser(){
		   openPage(null);
	 }
	
     public  void enterValidCredential(String uname,String pwd) {
		 
		 username.sendKeys(uname);
		 password.sendKeys(pwd);
		 loginbutton.click();
		 waitForNotVisible("login.loading.loader");
		
		 
	 }
     
     public void logOutNest(){
    	waitForNotVisible("login.loading.loader");
    	logout.click();
    	waitForNotVisible("login.loading.loader");
     }

     
     
     
     
	
	
	
	

}
