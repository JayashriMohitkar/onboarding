package com.qmetry.qaf.example.pages;





import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import static com.qmetry.qaf.automation.step.CommonStep.*;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.utilities.CommonUtils;

public class UIpostPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	CommonUtils commonutil=new CommonUtils();
	
	@FindBy(locator="publisher.loc")
	private QAFWebElement publishermenu;
	
	@FindBy(locator="verifylabel.loc")
	private List<QAFWebElement> labels;
	
	@FindBy(locator="title.postui.loc")
	private QAFWebElement pagetitle;
	
	@FindBy(locator="mypost.submenu.loc")
	private QAFWebElement mypostsubmenu;
	
	@FindBy(locator="tablecontent.loc")
	private List<QAFWebElement> verifytablecontent;
	
	@FindBy(locator="pagination.loc")
	private List<QAFWebElement> pagination;
	
	@FindBy(locator="hidefilter.loc")
	private QAFWebElement hidefilterelement;
	
	@FindBy(locator="createbutton.loc")
	private QAFWebElement createButton;
	
	@FindBy(locator="action.column.loc")
	private QAFWebElement actioncolumn;
	
	@FindBy(locator="editicon.loc")
	private QAFWebElement editicon;
	
	@FindBy(locator="search.button.loc")
	private QAFWebElement searchbutton;
	
	@FindBy(locator="reset.button.loc")
	private QAFWebElement resetbutton;
	
	@FindBy(locator="deleteicon.loc")
	private QAFWebElement deleteicon;
	
	@FindBy(locator="breadcrumb.home.loc")
	private QAFWebElement homebreadcrumb;
	
	@FindBy(locator="breadcrumb.publisher.loc")
	private QAFWebElement publisherbreadcrumb;
	
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
		
	}
	
	public void clickOnPublisher() {
		commonutil.clickOnElement(publishermenu);
		
	}
	
	public void clickOnMyPost(){
		commonutil.clickOnElement(mypostsubmenu);
	}
	
	
	
	public void verifyPostUIPage(String elements){
		String[] postuipage= elements.split(",");
		for(int i=0;i<postuipage.length;i++){
			Validator.verifyThat(new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("tablecontent.loc"),postuipage[i])).isPresent(),Matchers.equalTo(true) );
			com.qmetry.qaf.automation.util.Reporter.logWithScreenShot("verify UI page");
	     }
		

	}
	
	public void verifylabels(String elements){
		String[] ele = elements.split(",");
		int labelslist = labels.size();
		System.out.println(labelslist);
		for(int i=0;i<labelslist;i++){
			 WebElement labelelements = labels.get(i);
			 String str = labelelements.getText();
			 Reporter.log(str);
			Assert.assertEquals(str,ele[i]);
		}
	}
	
	public void pagetitle(){
		Validator.verifyThat("Verifying Title of page", pagetitle.getText(),
	                Matchers.containsString("My Posts")); 
	}
	
	public void verifypagination(){
		int size = pagination.size();
		if(size>0){
			verifyPresent("pagination.loc");
		}
		
	}
	
	public void verifyCreateButton(){
		
		Validator.verifyThat("Verifying Create button", createButton.getText(),
                Matchers.containsString("Create")); 
	}
	
	public void actionColumn(){
		
		Validator.verifyThat(actioncolumn.isPresent(),Matchers.equalTo(true)); 
		Validator.verifyThat(editicon.isDisplayed(),Matchers.equalTo(true));
		Validator.verifyThat(deleteicon.isDisplayed(),Matchers.equalTo(true));
	}
	
	public void verifySearchResetButton(){
		Validator.verifyThat("Verifying Search button", searchbutton.getText(),
                Matchers.containsString("Search"));
		
		Validator.verifyThat("Verifying Reset Button", resetbutton.getText(),
                Matchers.containsString("Reset"));
	}
	
	public void verifyHidenIcon(){
		Validator.verifyThat(hidefilterelement.isPresent(),Matchers.equalTo(true));
	}
	
	public void bredcrumbs(){
		String publisher = publisherbreadcrumb.getText();
		String home = homebreadcrumb.getText();
		String breadcrumb = home.concat(publisher);
		Validator.verifyThat("Verifying breadcrumb", breadcrumb,
                Matchers.containsString("Home  / Publisher"));
		
	}
	
	public NewPostUIPage clickOnCreateButton(){
		createButton.click();
		return new NewPostUIPage();
	}
	

}
