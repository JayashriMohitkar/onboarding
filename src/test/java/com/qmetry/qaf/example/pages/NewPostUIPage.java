package com.qmetry.qaf.example.pages;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class NewPostUIPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator="createpost.loc")
	private QAFWebElement createpost;
	
	@FindBy(locator="title.text.loc")
	private QAFWebElement titletext;
	
	@FindBy(locator="posturl.text.loc")
	private QAFWebElement posturltext;
	
	@FindBy(locator="imgurl.text.loc")
	private QAFWebElement imageurltext;
	
	@FindBy(locator="description.text.loc")
	private QAFWebElement descriptiontext;
	
	@FindBy(locator="selectlocation.loc")
	private QAFWebElement selectlocation;
	
	@FindBy(locator="selectcatogory.loc")
	private QAFWebElement selectcatogory;
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}
	
	public void verifyTitle(){
		Validator.verifyThat("Verifying Title", createpost.getText() ,
                Matchers.containsString("Create Post"));
	}
	
	public void verifyTextField(){
		Validator.verifyThat("Verifying Posturltext", titletext.getText() ,
                Matchers.containsString("Title*"));
		
		Validator.verifyThat("Verifying TitleText", posturltext.getText() ,
                Matchers.containsString("Post URL*"));
		
		Validator.verifyThat("Verifying imageurltext", imageurltext.getText() ,
                Matchers.containsString("Image URL"));
		
		Validator.verifyThat("Verifying descriptiontext", descriptiontext.getText() ,
                Matchers.containsString("Description*"));
	}
	
	public void verifyDropDown(){
		selectlocation.click();
		Validator.verifyThat("Verifying descriptiontext", selectlocation.getText() ,
                Matchers.containsString("Select Location"));
		Reporter.logWithScreenShot("dropdown present for location");
		
		selectcatogory.click();
		Validator.verifyThat("Verifying descriptiontext", selectcatogory.getText() ,
                Matchers.containsString("Select Category"));
		Reporter.logWithScreenShot("dropdown present for catogary");
	}

}
