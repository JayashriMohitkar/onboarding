package com.qmetry.qaf.example.pages;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.utilities.CommonUtils;

public class AddTravelRequestPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	CommonUtils commonutil=new CommonUtils();
	 
	@FindBy(locator="travelmenu.loc")
	public QAFWebElement travelmainmenu;
	
	@FindBy(locator="mytravelrequestmenu.loc")
	public QAFWebElement travelrequestmenu;
	
	@FindBy(locator="newTravelRequestButton.loc")
	public QAFWebElement newtravelbutton;
	
	
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
		
	}
	public void clickOnTravelMenu(){
		travelmainmenu.click();
		commonutil.clickOnElement(travelrequestmenu);
		 waitForNotVisible("login.loading.loader");
		newtravelbutton.click();
		waitForNotVisible("login.loading.loader");
	   }
	
	public void verifyAddTravelRequestPage(String elements){
		waitForNotVisible("login.loading.loader");
		String[] coloumnlist= elements.split(",");
		for(int i=0;i<coloumnlist.length;i++){
			Validator.verifyThat(new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("getalltitle.travelrequest.loc"),coloumnlist[i])).isPresent(),Matchers.equalTo(true) );
			com.qmetry.qaf.automation.util.Reporter.logWithScreenShot("Verify Add Travel Request Page");
	     }
		
	}

}
