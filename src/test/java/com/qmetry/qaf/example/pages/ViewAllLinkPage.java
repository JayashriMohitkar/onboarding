package com.qmetry.qaf.example.pages;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.step.CommonStep;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class ViewAllLinkPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		
		
	}
	
	
	 public void enterValidCredential(String username,String password) throws InterruptedException{
		 
		 driver.navigate().refresh();
		 sendKeys(username,"login1.userName.txt");
		 sendKeys(password,"login1.Password.txt");
		 }
	 
	
	 public void loginButton(){
		 waitForNotVisible("login.loading.loader");
		 click("login1.Logbutton.button");
		 waitForNotVisible("login.loading.loader");
	 }
	 
	
	 public void loginSuccessful(){
		Reporter.log("login should be successful"); 
		
	 }
	 
	 
	 public void clickOnMyViewButton(){
		 waitForNotVisible("login.loading.loader");
		  click("img.myview.loc");
		 waitForNotVisible("login.loading.loader");
	 }
	 
	
	 public  void clickOnViewAllTab(){
		 waitForNotVisible("login.loading.loader");
		click("homepage.viewall.loc");
	 }
	 
	 
	 public  void navigateToRespectivePage(){
		 
		 waitForNotVisible("login.loading.loader");
		 Reporter.log("user navigate to 'my leave list' page");
		Validator.verifyThat("Verifying Leave list Title", CommonStep.getText("homepage.leavelist.loc"),
                 Matchers.containsString("Team Leave List")); 
		navigateToHomeTab();
	}
	    
	  public void navigateToExpencePage(){
		 click("homepage.expence.loc");
		 clickOnViewAllTab();
		Reporter.log("user should be navigate to 'my expense list' page");
		 Validator.verifyThat("Verifying Expense List Title", CommonStep.getText("homepage.expenceTitle.loc"),
                 Matchers.containsString("Team Reimbursement List")); 
		 navigateToHomeTab();
	 }
	  
	  public void navigateToTravelListPage(){
		 click("homepage.travel.loc");
		 clickOnViewAllTab();
		Reporter.log("user should be navigate to 'My Travel Requests ' page");
		 Validator.verifyThat("Verifying My Travel Requests  Title", CommonStep.getText("homepage.travelTitle.loc"),
                 Matchers.containsString("Travel Requests")); 
		 navigateToHomeTab();
	 }
	 
	  public void navigateToHomeTab(){
		  waitForNotVisible("login.loading.loader");
		 click("homepage.home.loc");
		 waitForNotVisible("login.loading.loader");
	  }
	
	 

}
