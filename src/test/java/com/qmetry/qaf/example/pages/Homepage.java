package com.qmetry.qaf.example.pages;



import org.openqa.selenium.JavascriptExecutor;


import static com.qmetry.qaf.automation.step.CommonStep.*;


import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;

import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;



public class Homepage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
 
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
		
	}
	
	public void clickOnNavigable() {
		 waitForNotVisible("login.loading.loader");
		 QAFWebElement element1=new QAFExtendedWebElement("home.navigate1.link");
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", element1);
	}
	
	public void clickOnLeaveModule(){
		QAFWebElement element2=new QAFExtendedWebElement("home.leavelist1.link");
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", element2);
	}
	
	public void clickOnApplyLeaveModule() {
		click("applyleave.loc");
	}
	
	public void selectDateFrom() {
		click("datefrom.loc");
		click("dateselectfrom.loc");
		
    }
	
	public void selectDateTo() {
		click("dateTo.loc");
		click("dateselectTo.loc");
		
	}
	
	public void dropDown() {
		 waitForNotVisible("login.loading.loader");
		 verifyPresent("dropdown.loc");
		waitForNotVisible("login.loading.loader");
		click("dropdown.loc");
		click("selectdropdown.family.loc");
	}
	
	public void dayType() {
		click("FullDay.loc");
		click("applybutton.loc");
	}
	
	
	public void clickOnMyView()   {
		 waitForNotVisible("login.loading.loader");
		waitForVisible("homepage.myview.loc");
	      click("homepage.myview.loc");
	    waitForNotVisible("login.loading.loader");
   	 }
	
	public void clickOnApply(){
		click("applyleave.button.loc");
		 waitForNotVisible("login.loading.loader");
	}
	
	public void verifySuccessfullyApplyToLeave(){
		verifyPresent("msg.notification.loc");
	}
	
}






