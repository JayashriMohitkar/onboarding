package com.qmetry.qaf.example.pages;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;


import static com.qmetry.qaf.automation.step.CommonStep.*;



import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;


public class ExpenseRequestApprovalPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	JavascriptExecutor jse=(JavascriptExecutor) new WebDriverTestCase().getDriver();

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		
		
	}
	
	public void clickOnReimbursement(){
		 waitForNotVisible("login.loading.loader");
		 QAFWebElement Reimbursement=new QAFExtendedWebElement("Reimbursement.loc");
		 jse.executeScript("arguments[0].scrollIntoView();", Reimbursement);
		 Reimbursement.click();
		
		 
	}
	
	public void clickOnMyExpenceList(){
		
		 QAFWebElement myexpenceList=new QAFExtendedWebElement("myexpencelist.loc");
		 jse.executeScript("arguments[0].scrollIntoView();", myexpenceList);
		 QAFWebElement element1=new QAFExtendedWebElement("myexpencelist.loc");
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", element1);
		
			 
	}
	
	public void clickOnNewExpenceButton(){
		waitForNotVisible("login1.loading.loader");
		click("newexpencebutton.loc");
		 waitForNotVisible("login1.loading.loader");
	}
	
	public void fillValideDetails(){
		sendKeys("HomeBill", "title.loc");
		click("ExpenceCatogary.loc");
	    click("expenxeCatagary1111.loc");
		click("projectExpenceSelect.loc");
	    click("nestTest.project.loc");
		sendKeys("3000", "currency.ExpenceSelect.loc");
		click("submit.expencebutton.loc");
        Validator.verifyThat("Verifying Expense submitted successfully Title", CommonStep.getText("expencesuccesful.loc"),
                Matchers.containsString("Expense submitted successfully.")); 
		
	}
	
	public void expenceRequest(){
		click("homepage.expence.loc");
		click("homepage.viewall.loc");
        waitForNotVisible("login1.loading.loader");
		 click("titleTable.expence.loc");
		 waitForNotVisible("login1.loading.loader");
		 sendKeys("approved", "managerText.loc");
		 click("approve.expencebutton.loc");
		 waitForNotVisible("login1.loading.loader");
		Reporter.logWithScreenShot("functionality of expense request approval");
	}
	 
	public void redirectedtoMyExpenseList() {
		clickOnReimbursement();
		clickOnMyExpenceList();
	}
	
	public void fillInvalideDetails() {
		sendKeys(" ", "title.loc");
		click("ExpenceCatogary.loc");
		sendKeys(" @@", "projectExpenceSelect.loc");
	     sendKeys("###", "currency.ExpenceSelect.loc");
		click("submit.expencebutton.loc");
		Validator.verifyThat("Error msg varify", CommonStep.getText("errorMsg.loc"),
                Matchers.containsString("This is a required field.")); 
		
	}
	
	public void fillDataWithFileAttachment() throws Exception {
		sendKeys("HomeBill", "title.loc");
		click("ExpenceCatogary.loc");
	    click("expenxeCatagary1111.loc");
	    sendKeys("3000", "currency.ExpenceSelect.loc");
		click("attachment.loc");
		Runtime.getRuntime().exec("C:\\Users\\Jayashri.Mohitkar\\Documents\\uploadfile1.exe");
		Reporter.logWithScreenShot("file attachment successfully");
		click("projectExpenceSelect.loc");
		click("otherbutton.loc");
        click("submit.expencebutton.loc");
	}
	

}
