package com.qmetry.qaf.example.pages;


import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;

import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.utilities.CommonUtils;

public class LeaveHolidayPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
CommonUtils commonutil=new CommonUtils();
	
	@FindBy(locator="home.navigate1.link")
	private QAFWebElement homenavigate;
	
	@FindBy(locator="home.leavelist1.link")
	private QAFWebElement leavemenu;
	
	@FindBy(locator="applyleave.loc")
	private QAFWebElement applyleave;
	
	@FindBy(locator="datefrom.loc")
	private QAFWebElement datefrom;
	
	@FindBy(locator="dateTo.loc")
	private QAFWebElement dateto;
	
	@FindBy(locator="dropdown.loc")
	private QAFWebElement selectreason;
	
	@FindBy(locator="selectdropdown.family.loc")
	private QAFWebElement reasontype;
	
	@FindBy(locator="applyleave.button.loc")
	private QAFWebElement applybutton;
    
	@FindBy(locator="msg.notification.loc")
	private QAFWebElement confirmmessage;


	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}
	
	public void clickOnNavigable(){
		 waitForNotVisible("login.loading.loader");
		 commonutil.clickOnElement(homenavigate);
	}
	
	public void clickOnLeaveModule() {
		commonutil.clickOnElement(leavemenu);
	}
	
	public void clickOnApplyLeaveModule() {
		commonutil.clickOnElement(applyleave);
		 waitForNotVisible("login.loading.loader");
	}
	
	
	
	public void selectDate() {
		datefrom.clear();
		datefrom.sendKeys("27-July-2018");
		dateto.clear();
		dateto.sendKeys("30-July-2018");
		waitForNotVisible("login.loading.loader");
		
	}
	
	public void leaveReason() {
		selectreason.click();
		reasontype.click();
	}
	
	public void clickOnApply(){
		applybutton.click();
	}
	
	public void getnotification(){
		Reporter.log(confirmmessage.getText());
		Validator.verifyThat("Verifying notification", confirmmessage.getText() ,
                Matchers.containsString("You have already applied/taken for selected days."));
	}
	

}
