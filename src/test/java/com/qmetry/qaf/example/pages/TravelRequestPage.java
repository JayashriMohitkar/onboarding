package com.qmetry.qaf.example.pages;

import static com.qmetry.qaf.automation.step.CommonStep.*;







import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Reporter;



public class TravelRequestPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		
	}
	
     public  void enterValidCredential(String username,String password) {
		
		 driver.navigate().refresh();
		 sendKeys(username,"login1.userName.txt");
		 sendKeys(password,"login1.Password.txt");
		 click("login1.Logbutton.button");
		 waitForNotVisible("login1.loading.loader");
		
	}
 
    
    public void clickOnMyViewButton1(){
    	 waitForNotVisible("login.loading.loader");
    	 click("img.myview.loc");
		 waitForNotVisible("login.loading.loader");
	 }
    
    public void clickOnTravelTab(){
    	waitForNotVisible("login.loading.loader");
		 click("homepage.travel.loc");
		 waitForNotVisible("login.loading.loader");
		
	 }
    
    public void travelRequestRow(){
    	new QAFExtendedWebElement("img.travellist.loc").click();
    	waitForNotVisible("login.loading.loader");
	 }
    
    public void verifyThreeOptions(){
    	 waitForNotVisible("login.loading.loader");
         verifyPresent("img.row.button");
    	Reporter.logWithScreenShot("verify travel request page");
    }

}
