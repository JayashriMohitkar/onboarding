package com.qmetry.qaf.example.pages;




import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;



import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import static com.qmetry.qaf.automation.step.CommonStep.*;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;

import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.example.utilities.CommonUtils;



public class TeamLeaveListPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	
	CommonUtils commonutil=new CommonUtils();
	

	
  public void clickOnTeamLeaveList() {
		
		QAFWebElement teamleave=new QAFExtendedWebElement("TeamLeaveList.loc");
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", teamleave);
	  }
	
	
	
	public void verifycoloum(String elements){
		String[] coloumncontent= elements.split(",");
		for(int i=0;i<coloumncontent.length;i++){
			Validator.verifyThat(new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("listofelement.allfiel.loc"),coloumncontent[i])).isPresent(),Matchers.equalTo(true) );
		
	     }
		

	}
        
  
	
	public void scrollDownMenu() throws Exception{
		commonutil.scrollwindow();
	}
	
	public void submitAndApproveButton(){
		waitForNotVisible("login.loading.loader");
		verifyPresent("submitbutton1.loc");
		verifyPresent("approveall.loc");
	}



	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
		
	}

}
