package com.qmetry.qaf.example.pages;
import static com.qmetry.qaf.automation.step.CommonStep.*;


import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;



import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;


public class BrightSparkPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	


	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		
	}
	
	public void scrollDownForRandR(){
		waitForNotVisible("login.loading.loader");
		 JavascriptExecutor jse=(JavascriptExecutor) new WebDriverTestCase().getDriver();
		 QAFWebElement subscroll=new QAFExtendedWebElement("RandR.loc");
		 jse.executeScript("arguments[0].scrollIntoView();", subscroll);
		subscroll.click();
		
	}
  
	public void clickOnNominateModule(){
		JavascriptExecutor jse=(JavascriptExecutor) new WebDriverTestCase().getDriver();
		 QAFWebElement nominate=new QAFExtendedWebElement("RandR.nominate.loc");
		 jse.executeScript("arguments[0].scrollIntoView();", nominate);
		 jse.executeScript("arguments[0].click();", nominate);
	}
	
	
	public void clickOnSparkModule(){
		JavascriptExecutor jse=(JavascriptExecutor) new WebDriverTestCase().getDriver();
		 QAFWebElement brightsparkElement=new QAFExtendedWebElement("brightspark.loc");
		 jse.executeScript("arguments[0].scrollIntoView();", brightsparkElement);
		   jse.executeScript("arguments[0].click();", brightsparkElement);
		 
		
	}
	
	
	
	public void clickOnRandRManagerView() {
		waitForNotVisible("login1.loading.loader");
		click("RandR.managerview.loc");
		click("RandR.Request.loc");
		waitForNotVisible("login1.loading.loader");
		verifyPresent("tushitmanager.virat.loc");
	 }
	
	public void observeListRnRList() {
		click("RandRDelivery.manager.loc");
		JavascriptExecutor jse=(JavascriptExecutor) new WebDriverTestCase().getDriver();
		   QAFWebElement RnRRequestelement=new QAFExtendedWebElement("RandR.Request.loc");
		   jse.executeScript("arguments[0].scrollIntoView();", RnRRequestelement);
		RnRRequestelement.click();
		waitForNotVisible("login1.loading.loader");
		Validator.verifyThat("verify Virat Kohali name", CommonStep.getText("virat.observe.loc"),
                Matchers.containsString("Virat Kohali")); 
		waitForNotVisible("login.loading.loader");
		
	}
	public void verifyaprroveReject(){
		waitForNotVisible("login.loading.loader");
	    verifyPresent("approve.rejectbutton.loc");
	}
	
	public void nomineeVirat() {
		waitForNotVisible("login1.loading.loader");
		click("selectAcard.loc");
		click("nominee.loc");
		sendKeys("virat", "searchfield.nominee.loc");
		click("nominee.virat.loc");
		waitForNotVisible("login1.loading.loader");
	    click("projectfield.loc");
		sendKeys("nest", "searchfield.nominee.loc");
		click("projectname.nestpage.loc");
		sendKeys("Difficult test case", "challanging.situation.loc");
	   sendKeys("Any benefits", "Benefitsaccrued.loc");
	   sendKeys("not provided", "solution.provided.loc");
	   JavascriptExecutor jse=(JavascriptExecutor) new WebDriverTestCase().getDriver();
	   QAFWebElement RationaleforNomination=new QAFExtendedWebElement("RationaleforNomination.loc");
	   jse.executeScript("arguments[0].scrollIntoView();", RationaleforNomination);
	   RationaleforNomination.sendKeys("I don't want any contribution");
	   click("postbutton.loc");
	   waitForNotVisible("login.loading.loader");
	}
	
	
	
	public void verifyRow(String elements){
		String[] elements2= elements.split(",");
		for(int i=0;i<elements2.length;i++){
			Validator.verifyThat(new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("tworow.brightspark.loc"),elements2[i])).isPresent(),Matchers.equalTo(true) );
			com.qmetry.qaf.automation.util.Reporter.logWithScreenShot("Verify Row content");
		}
	}
	
	public void verifyValueAdditionContent(String valueadditions){
		String[] valueaddition1= valueadditions.split(",");
		for(int i=0;i<valueaddition1.length;i++){
			Validator.verifyThat(new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("valueAdditons.elements.loc"),valueaddition1[i])).isPresent(),Matchers.equalTo(true) );
			com.qmetry.qaf.automation.util.Reporter.logWithScreenShot("Verify Value Addition Content");
	     }
	}
	
	public void rewardCardImage(){
		  QAFWebElement rewardcardimage=new QAFExtendedWebElement("imageReward.card.loc");
		  Boolean ImagePresent = (Boolean) ((JavascriptExecutor)driver).executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", rewardcardimage);
		    if (!ImagePresent)
		    {
		    	
		         Reporter.log("Image not displayed.");
		         
		    }
		    else
		    {
		    	verifyPresent("imageReward.card.loc");
		        Reporter.log("Image displayed.");
		    }
		
	}
	
	public void verifyBackButton(){
		Validator.verifyThat("verify back button", CommonStep.getText("backbutton.loc"), Matchers.containsString("Back"));
	}
	
	public void clickOnApprovedButton(){
		waitForNotVisible("login1.loading.loader");
		waitForVisible("approved.button.loc");
		click("approved.button.loc");
		sendKeys("approved", "approval.comments.loc");
		click("commentbox.submitbutton.loc");
		waitForNotVisible("login1.loading.loader");
	}
	
	public void clickOnRnR(){
		click("Hrmangr.RnR.loc");
		click("Hrgaurav.RnRrequest.loc");
		waitForNotVisible("login1.loading.loader");
		click("Hrgaurav.BStablelist.loc");
	}
	public void clickOnRejectButton(){
		waitForVisible("rejectsp.button.loc");
		click("rejectsp.button.loc");
		sendKeys("approved", "approval.comments.loc");
		click("commentbox.submitbutton.loc");
		waitForNotVisible("login1.loading.loader");
	}
	
	public void clickOnRejectStatus(){
		waitForNotVisible("login1.loading.loader");
		click("employeeName.loc");
		sendKeys("virat", "searchtext.empname.loc");
		click("virat.empname.loc");
		waitForNotVisible("login1.loading.loader");
		click("status.loc");
		click("rejected.status.loc");
		click("group.loc");
		click("selectall.group.loc");
		click("rewards.loc");
		click("brightspark.reward.loc");
		click("searchbuttonRnR.loc");
		waitForNotVisible("login1.loading.loader");
		click("brightspark.rejectstatus.loc");
	}
	
	public void verifyList() {
		waitForNotVisible("login1.loading.loader");
		click("RandR.managerview.loc");
		click("RandR.Request.loc");
		waitForNotVisible("login1.loading.loader");
		click("RandRTableRequest.list.loc");
	}
	
	public void clickOnListBrightSpark(){
		waitForVisible("Hrgaurav.BStablelist.loc");
		CommonStep.click("Hrgaurav.BStablelist.loc");
	}
	
}
