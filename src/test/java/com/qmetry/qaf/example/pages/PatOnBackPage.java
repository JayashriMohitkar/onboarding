package com.qmetry.qaf.example.pages;
import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;

import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.example.utilities.CommonUtils;

public class PatOnBackPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	CommonUtils commonutil=new CommonUtils();
	
	@FindBy(locator="patonback.loc")
	private QAFWebElement patonbackmenu;
	
	@FindBy(locator="patOn.image.loc")
	private QAFWebElement patonimage;
	
	@FindBy(locator="selcenominee.paton.loc")
	private QAFWebElement selectnominee;
	
	@FindBy(locator="searchnominee.paton.loc")
	private QAFWebElement searchnomineetextbox;
	
	@FindBy(locator="viratnominee.paton.loc")
	private QAFWebElement nomineeasvirat;
	
	@FindBy(locator="selectproject.paton.loc")
	private QAFWebElement selectproject;
	
	@FindBy(locator="searchproject.paton.loc")
	private QAFWebElement searchprojectfield;
	
	@FindBy(locator="projectname.paton.loc")
	private QAFWebElement projectname;
	
	@FindBy(locator="contribution.paton.loc")
	private QAFWebElement contibutiontextbox;
	
	@FindBy(locator="postbutton.paton.loc")
	private QAFWebElement postbutton;
	
	@FindBy(locator="nominatesuccess.paton.loc")
	private QAFWebElement nominatesuccess;
	

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		
		
	}
	
	public void clickOnPatOnBack(){
		commonutil.clickOnElement(patonbackmenu);
	}
   
	public  void fillDataOnPatOnPage() {
		patonimage.click();
		selectnominee.click();
		searchnomineetextbox.sendKeys("virat");
		nomineeasvirat.click();
		selectproject.click();
		searchprojectfield.sendKeys("nest");
		projectname.click();
		contibutiontextbox.sendKeys("not available");
		commonutil.scrollForVisibleElement(postbutton);
		postbutton.click();
		 waitForNotVisible("login.loading.loader");
		
		
	}
}
