package com.qmetry.qaf.example.steps;


import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.util.Reporter;

import com.qmetry.qaf.example.pages.ViewAllLinkPage;



public class ViewAllLink extends WebDriverBaseTestPage<WebDriverTestPage>{

	ViewAllLinkPage viewlink=new ViewAllLinkPage();
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		
				
	}
	
	 @QAFTestStep(description="user able to launch the browser")
	 public void userAbleToLaunchTheBrowser(){
		   driver.get("/");
			driver.manage().window().maximize();
		
	 }
	 
	 @QAFTestStep(description="user can enter userName as {username} and enter password as {password}")
	 public void userCanEnterUserNameAndPassword(String username,String password) throws InterruptedException{
		 
		 viewlink.enterValidCredential(username, password);
	 }
	 
	 @QAFTestStep(description="user should be click on login button")
	 public void userShouldBeClickOnLoginButton(){
		
		  viewlink.loginButton();
	 }
	 
	 @QAFTestStep(description="user login successfully")
	 public void userLoginSuccessfully(){
		 
        viewlink.loginSuccessful();
	 }
	 
	 @QAFTestStep(description="user click on 'my view' tab")
	 public void userClickOnMyViewButton() throws Exception{
		 waitForNotVisible("login.loading.loader");
		 viewlink.clickOnMyViewButton();
		 waitForNotVisible("login.loading.loader");
	 }
	 
	 @QAFTestStep(description="user should be click on 'ViewAll' link")
	 public void userShouldBeClickOnViewallTab(){
	    viewlink.clickOnViewAllTab();
	 }
	 
	 @QAFTestStep(description="user should be navigate to respective page")
	 public void userShouldBeNavigateToRespectivePage(){
		 
		 viewlink.navigateToRespectivePage();
		 viewlink.navigateToExpencePage();
		 viewlink.navigateToTravelListPage();
		 Reporter.logWithScreenShot("functionality of the View All link on the request section");
	 }
	 
	 
	 
}
