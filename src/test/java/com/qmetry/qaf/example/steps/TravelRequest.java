package com.qmetry.qaf.example.steps;

import static com.qmetry.qaf.automation.step.CommonStep.waitForNotVisible;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.pages.TravelRequestPage;

public class TravelRequest extends WebDriverBaseTestPage<WebDriverTestPage> {

	   TravelRequestPage travelpage=new TravelRequestPage();
	
	 @QAFTestStep(description="user can able to launch the browser")
	 public void userCanAbleToLaunchTheBrowser(){
		 driver.get("/");
			driver.manage().window().maximize();
		
	 }
	 
	  @QAFTestStep(description="user should be enter userName as {0} and enter password as {1}")
	 public void userShouldBeEnterUserNameAsAndEnterPassword(String username,String password) throws InterruptedException{
		  travelpage.enterValidCredential(username, password);
		
	 }
	 
	 
	 @Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		
	}
	 
	 @QAFTestStep(description="user click on 'my view' button1")
	 public void userClickOnMyViewButton1(){
		 waitForNotVisible("login.loading.loader");
		travelpage.clickOnMyViewButton1();
		waitForNotVisible("login.loading.loader");
	 }
	 
	 @QAFTestStep(description="user should be Click on 'Travel' tab")
	 public void userShouldBeClickOnTravelTab(){
		 waitForNotVisible("login.loading.loader");
		 travelpage.clickOnTravelTab();
		
	 }

	
	 
	 @QAFTestStep(description="user Click on request row in the Travel request tab")
	 public void userClickOnRequestRowInTheTravelRequestTab(){
		travelpage.travelRequestRow();
	 }
	 
	 @QAFTestStep(description="user should be verify three option on the Travel request row")
	 public void userShouldBeVerifyThreeOptionOnTheTravelRequestRow(){
		 travelpage.verifyThreeOptions();
	 }

	

}
