package com.qmetry.qaf.example.steps;
import static com.qmetry.qaf.automation.step.CommonStep.*;



import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;

import com.qmetry.qaf.automation.util.Reporter;

public class NestLogin extends WebDriverBaseTestPage<WebDriverTestPage>{

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		
			
	}
	
	 @QAFTestStep(description="user launch the browser")
	 public void userLaunchTheBrowser(){
		 driver.get("/");
		 driver.manage().window().maximize();	
		
	 }
	 
	  @QAFTestStep(description="user enter userName as {username} and enter password as {password}")
	 public void userEnterUserNameAndEnterPassword(String username,String password) throws InterruptedException{
		 
		 driver.navigate().refresh();
		 sendKeys(username,"login.userName.txt");
		 sendKeys(password,"login.Password.txt");
		 click("login.Logbutton.button");
		 waitForNotVisible("login.loading.loader");
		 
	}
	 
	 @QAFTestStep(description="user should be displayed {errormsg}")
	 public void UserShouldBeDisplayed(String Errormsg){
		Reporter.log(Errormsg);
	 }
	 
	 @QAFTestStep(description="user should be logout successfully")
	 public void userShouldBeLogoutSuccessfully(){
		 waitForNotVisible("login.loading.loader");
	     click("login.logoutbutton.button");
		
		 
	 }
	 
}
