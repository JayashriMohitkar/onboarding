package com.qmetry.qaf.example.utilities;

import org.openqa.selenium.JavascriptExecutor;

import org.openqa.selenium.WebElement;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;

public class CommonUtils extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	
	public static void switchToWindow(){
		String parentwindow = new WebDriverTestBase().getDriver().getWindowHandle();
		for(String  window :new WebDriverTestBase().getDriver().getWindowHandles()){
			if(!window.equals(parentwindow)){
				new WebDriverTestBase().getDriver().switchTo().window(parentwindow);
			}
		}
	}

	
     public  void waitforloader(){
  	 CommonStep.waitForNotPresent("login.loading.loader");
  	 
   }
   
   public  void scrollwindow() {
  	 JavascriptExecutor js = (JavascriptExecutor) driver;
  	 js.executeScript("window.scrollBy(0,1000)");
   }
   
   public void clickOnElement(WebElement element){
	   JavascriptExecutor jse=(JavascriptExecutor) new WebDriverTestCase().getDriver();
	   jse.executeScript("arguments[0].click();", element);
   }
   
   public void scrollForVisibleElement(WebElement element){
	   JavascriptExecutor jse=(JavascriptExecutor) new WebDriverTestCase().getDriver();
		jse.executeScript("arguments[0].scrollIntoView();", element);
   }


@Override
protected void openPage(PageLocator locator, Object... args) {
	// TODO Auto-generated method stub
	
}
	
}
